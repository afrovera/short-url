# AWS serverless short-url API

Original concept (https://aws.amazon.com/jp/blogs/compute/build-a-serverless-private-url-shortener/)

After a successful checkout, change into the proper directory. You should see one cloudformation template in json format.

```shell
git clone https://gitlab.com/afrovera/short-url.git && cd short-url
```

Prerequisites:
Administrative IAM user

If you try to run deploy this template and you have insufficient IAM permissions, it will fail. Run this update command while logged in with administrative IAM user or IAM user with granular permissions for services declared in this template.

For simplicity the example below will deploy a stack in us-east-1 region which is global region for Cloudfront. Change the values for parameters to desired globally unique bucket names for Cloudfront logs and bucket that holds the files for the API. If left empty as-is, the template will generate unique bucket names for you.

```shell
aws cloudformation deploy \
    --template-file short-url.json  \
    --capabilities CAPABILITY_IAM \
    --stack-name my-short-url \
    --parameter-overrides "CloudFrontLogsBucketName=" "S3BucketName=" \
    --region us-east-1
```
Once the stack is deployed, you should see the API endpoint as part of the outputs.

```shell
aws cloudformation describe-stacks --query Stacks[].Outputs[*].[OutputKey,OutputValue] --output text
```

Bonus:

Setup custom domain and ACM certificate for your Cloudfront alias. 

Analyzing Cloudfront access logs with Glue, Athena and Quicksight. By analysing logs with these serverless tools we can get better insight into the performance of our Cloudfront trafic to and from short url API.

Original concept (https://github.com/aws-samples/amazon-cloudfront-log-analysis/blob/master/lab1-serveless-cloudfront-log-analysis/README.md)

Prerequisites:
Administrative IAM user
Cloudfront logs in S3 bucket

Setup authentication with Cognito (https://aws.amazon.com/blogs/networking-and-content-delivery/authorizationedge-how-to-use-lambdaedge-and-json-web-tokens-to-enhance-web-application-security/)